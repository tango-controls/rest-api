Tango REST API specification Contents
=====================================

.. toctree::
   :glob:
   :name: maintoc
   :maxdepth: 2

   index
   general
   hosts
   subscriptions
   device